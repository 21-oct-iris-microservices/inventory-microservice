package com.classpath.inventorymicroservice.service;

import lombok.RequiredArgsConstructor;
import lombok.extern.slf4j.Slf4j;
import org.springframework.kafka.annotation.KafkaListener;
import org.springframework.stereotype.Service;

@Service
@Slf4j
@RequiredArgsConstructor
public class OrderProcessor {

    @KafkaListener(topics = {"orders-topic"},groupId = "inventory")
    public void processOrder(String order){
      log.info("Processing the order to update the inventory :: ");
      log.info(order);
    }
}